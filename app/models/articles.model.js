module.exports = (sequelize, Sequelize) => {
    const Articles = sequelize.define("articles", {
      author: {
        type: Sequelize.STRING
      },
      title: {
        type: Sequelize.STRING
      },
      body: {
        type: Sequelize.STRING
      },
      created: {
        type: Sequelize.DATE,
        defaultValue: sequelize.literal('CURRENT_TIMESTAMP'),
      }
    }
    ,{
        timestamps: false
    });
    return Articles;
  };