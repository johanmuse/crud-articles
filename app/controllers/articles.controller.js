const db = require("../models");
const Articles = db.articles;
const Op = db.Sequelize.Op;
// Create and Save a new Articles
exports.create = (req, res) => {
  // Validate request
  if (!req.body.author) {
    res.status(400).send({
      message: "Author can not be empty!"
    });
    return;
  }
  if (!req.body.title) {
    res.status(400).send({
      message: "Title can not be empty!"
    });
    return;
  }
  if (!req.body.body) {
    res.status(400).send({
      message: "Body can not be empty!"
    });
    return;
  }
  // Create a Articles
  const articles = {
    author: req.body.author,
    title: req.body.title,
    body: req.body.body};
  // Save Articles in the database
  Articles.create(articles)
    .then(data => {
      res.send(data);
    })
    .catch(err => {
      res.status(500).send({
        message:
          err.message || "Some error occurred while creating the Articles."
      });
    });
};
// Retrieve all Articles from the database.
exports.findAll = (req, res) => {
    const author = req.query.author;
    const query = req.query.query;
    if(author){
        var condition = { author: { [Op.like]: `%${author}%` } };
    }
    else if(query){
        var condition = {
            [Op.or]: [
              {
                title: {
                  [Op.like]: `%${query}%`
                }
              },
              {
                body: {
                  [Op.like]: `%${query}%`
                }
              }
            ]
        };
    }else{
        var condition = null;
    }
    
    Articles.findAll({ order: [["created", "DESC"]], where: condition })
      .then(data => {
        res.send(data);
      })
      .catch(err => {
        res.status(500).send({
          message:
            err.message || "Some error occurred while retrieving tutorials."
        });
      });
};
// Find a single Articles with an id
exports.findOne = (req, res) => {
    const id = req.params.id;
    Articles.findByPk(id)
      .then(data => {
        if (data) {
          res.send(data);
        } else {
          res.status(404).send({
            message: `Cannot find Articles with id=${id}.`
          });
        }
      })
      .catch(err => {
        res.status(500).send({
          message: "Error retrieving Articles with id=" + id
        });
      });
};
// Update a Articles by the id in the request
exports.update = (req, res) => {
    const id = req.params.id;
    Articles.update(req.body, {
      where: { id: id }
    })
      .then(num => {
        if (num == 1) {
          res.send({
            message: "Articles was updated successfully."
          });
        } else {
          res.send({
            message: `Cannot update Articles with id=${id}. Maybe Articles was not found or req.body is empty!`
          });
        }
      })
      .catch(err => {
        res.status(500).send({
          message: "Error updating Articles with id=" + id
        });
      });
};
// Delete a Articles with the specified id in the request
exports.delete = (req, res) => {
    const id = req.params.id;
    Articles.destroy({
      where: { id: id }
    })
      .then(num => {
        if (num == 1) {
          res.send({
            message: "Articles was deleted successfully!"
          });
        } else {
          res.send({
            message: `Cannot delete Articles with id=${id}. Maybe Articles was not found!`
          });
        }
      })
      .catch(err => {
        res.status(500).send({
          message: "Could not delete Articles with id=" + id
        });
      });};
// Delete all Articles from the database.
exports.deleteAll = (req, res) => {
    Articles.destroy({
        where: {},
        truncate: false
      })
        .then(nums => {
          res.send({ message: `${nums} Articles were deleted successfully!` });
        })
        .catch(err => {
          res.status(500).send({
            message:
              err.message || "Some error occurred while removing all tutorials."
          });
    });
};
