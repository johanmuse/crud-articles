module.exports = app => {
    const articles = require("../controllers/articles.controller.js");
    var router = require("express").Router();
    // Create a new Articles
    router.post("/", articles.create);
    // Retrieve all Articles
    router.get("/", articles.findAll);
    // Retrieve a single Articles with id
    router.get("/:id", articles.findOne);
    // Update a Articles with id
    router.put("/:id", articles.update);
    // Delete a Articles with id
    router.delete("/:id", articles.delete);
    // Delete all Articles
    router.delete("/", articles.deleteAll);
    
    app.use('/api/articles', router);
  };