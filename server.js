const express = require("express");
const cors = require("cors");
const app = express();
var corsOptions = {
  origin: "http://localhost:8020"
};
app.use(cors(corsOptions));
// parse requests of content-type - application/json
app.use(express.json());
// parse requests of content-type - application/x-www-form-urlencoded
app.use(express.urlencoded({ extended: true }));

app.get("/", (req, res) => {
  res.json({ message: "Welcome to kumparan application." });
});

require("./app/routes/articles.routes")(app);


// set port, listen for requests
const PORT = process.env.PORT || 8020;
app.listen(PORT, () => {
  console.log(`Server is running on port ${PORT}.`);
});